const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const studentSchema=new Schema({
username:{
    type:String,
   
    trim:true,
    minLength:3
},
college:{
    type:String,
  
    trim:true,
    minLength:3
},
GPA:{
    type:Number,
   
    trim:true,
    minLength:3  
}

},{
    timestamps: true,
  });
 
const Student=mongoose.model('Student',studentSchema);

module.exports=Student;
